FROM node:10-alpine

WORKDIR /usr/app

COPY package.json .

RUN yarn

COPY poi.config.js .
COPY src/client src/client
RUN yarn run build-client

COPY backpack.config.js .
COPY src/server src/server
RUN yarn run build-server

ENV PORT 80

EXPOSE 80

CMD ["node", "build/main.js"]