import Vue from 'vue'
import App from './App.vue'
import router from './router'
import GlobalEvents from 'vue-global-events'
import VueProgressBar from 'vue-progressbar'

// register globally
Vue.component('GlobalEvents', GlobalEvents)
Vue.use(VueProgressBar, {})

export default new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
