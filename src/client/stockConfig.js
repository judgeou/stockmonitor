const config = [
  {
    match: '中证小盘500指数',
    symbol: 'SH000905', // 中证500
    weights: 0.1370
  },
  {
    match: '中证1000指数',
    symbol: 'SH000852', // 中证1000
    weights: 0.1379
  },
  {
    match: '中证红利指数',
    symbol: 'SH000922', // 中证红利
    weights: 0.0194
  },
  {
    match: '创业板指数',
    symbol: 'SZ399006', // 创业板
    weights: 0.0382
  },
  {
    match: '德国DAX指数',
    symbol: 'F000614', // 德国DAX
    weights: 0.1548
  },
  {
    match: '中证全指可选消费指数',
    symbol: 'SH000989', // 全指可选
    weights: 0.0281
  },
  {
    match: '中证全指医药卫生指数',
    symbol: 'SH000991', // 医药卫生
    weights: 0.0341
  },
  {
    match: '中证全指信息技术指数',
    symbol: 'SH000993', // 信息技术
    weights: 0.0551
  },
  {
    match: '中证环保产业指数',
    symbol: 'SH000827', // 环保产业
    weights: 0.1346
  },
  {
    match: '中证养老产业指数',
    symbol: 'SZ399812', // 养老产业
    weights: 0.0474
  },
  {
    match: '中证军工指数',
    symbol: 'SZ399967', // 中证军工
    weights: 0.0874
  },
  {
    match: '中证传媒指数',
    symbol: 'SZ399971', // 中证传媒
    weights: 0.1261
  }
]

export {
  config
}
