import { config } from './stockConfig'

const defaultText = `

隐藏持仓为0的基金
中证小盘500指数 2376.84元
13.64%
南方中证500量化增强C 002907
待确认金额 0.00元
持有份额 3012.47份（0.00份赎回中）
持有单位成本 0.9491元
当前净值 0.7890元（2018年11月05日 -0.13%）
持有收益 -482.16元（-16.86%）
中证1000指数 2413.59元
13.85%
招商中证1000指数增强C 004195
待确认金额 0.00元
持有份额 3539.50份（0.00份赎回中）
持有单位成本 0.8405元
当前净值 0.6819元（2018年11月05日 +0.63%）
持有收益 -561.41元（-18.87%）
中证红利指数 332.85元
1.91%
富国中证红利指数增强 100032
待确认金额 0.00元
持有份额 336.55份（0.00份赎回中）
持有单位成本 1.0073元
当前净值 0.9890元（2018年11月05日 -0.4%）
持有收益 -6.15元（-1.81%）
创业板指数 674.57元
3.87%
南方创业板ETF联接C 004343
待确认金额 0.00元
持有份额 1003.22份（0.00份赎回中）
持有单位成本 0.7227元
当前净值 0.6724元（2018年11月05日 +0.03%）
持有收益 -50.43元（-6.96%）
德国DAX指数 2612.40元
14.99%
华安德国30(DAX)ETF联接QDII 000614
待确认金额 0.00元
持有份额 2364.16份（0.00份赎回中）
持有单位成本 1.1772元
当前净值 1.1050元（2018年11月02日 +0.55%）
持有收益 -170.60元（-6.13%）
中证全指可选消费指数 492.98元
2.83%
广发中证全指可选消费ETF联接C 002977
待确认金额 0.00元
持有份额 818.22份（0.00份赎回中）
持有单位成本 0.6257元
当前净值 0.6025元（2018年11月05日 0.0%）
持有收益 -19.02元（-3.71%）
中证全指医药卫生指数 610.06元
3.50%
广发中证全指医药卫生ETF联接C 002978
待确认金额 0.00元
持有份额 856.71份（0.00份赎回中）
持有单位成本 0.7611元
当前净值 0.7121元（2018年11月05日 -0.28%）
持有收益 -41.94元（-6.43%）
中证全指信息技术指数 976.64元
5.60%
广发中证全指信息技术ETF联接C 002974
待确认金额 0.00元
持有份额 1291.68份（0.00份赎回中）
持有单位成本 0.8640元
当前净值 0.7561元（2018年11月05日 -0.11%）
持有收益 -139.36元（-12.49%）
中证环保产业指数 2364.22元
13.56%
广发中证环保产业指数C 002984
待确认金额 0.00元
持有份额 4681.63份（0.00份赎回中）
持有单位成本 0.6348元
当前净值 0.5050元（2018年11月05日 +1.24%）
持有收益 -607.78元（-20.45%）
中证养老产业指数 839.26元
4.82%
广发中证养老产业指数C 002982
待确认金额 0.00元
持有份额 990.98份（0.00份赎回中）
持有单位成本 0.9405元
当前净值 0.8469元（2018年11月05日 -0.2%）
持有收益 -92.74元（-9.95%）
中证军工指数 1520.72元
8.73%
前海开源中证军工指数C 002199
待确认金额 0.00元
持有份额 2790.32份（0.00份赎回中）
持有单位成本 0.5845元
当前净值 0.5450元（2018年11月05日 +0.37%）
持有收益 -110.28元（-6.76%）
中证传媒指数 2215.06元
12.71%
广发中证传媒ETF联接C 004753
待确认金额 0.00元
持有份额 3250.75份（0.00份赎回中）
持有单位成本 0.7760元
当前净值 0.6814元（2018年11月05日 -0.28%）
持有收益 -307.61元（-12.19%）

`

function getMatchConfig (text = defaultText) {
  const lines = text.split('\n')
  let totalMoney = 0
  lines.forEach((v, i) => {
    for (let c of config) {
      if (v.match(c.match + ' ')) {
        const cm = Number(v.match(/ (\d+(\.\d+)?)/)[0])
        const weights = Number(lines[i + 1].match(/(\d+(\.\d+)?)/)[0]) / 100
        console.log(`${v} | ${cm} | ${weights}`)
        totalMoney += cm
        c.weights = weights
      }
    }
  })
  if (text) {
    localStorage.setItem('matchText', text)
  }
  return { config, totalMoney }
}

export {
  getMatchConfig
}
