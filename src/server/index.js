const Express = require('express')
const bodyParser = require('body-parser')
const axios = require('axios')
// const browser = require('./brower')

const app = Express()
app.use(bodyParser.json())

app.use(Express.static('dist'))

// const wPage = browser.getPage()
// SZ399971,SZ399967,SZ399812,SH000827,SH000993,SH000991,SH000989,SH000922,SH000852,SH000905,SZ399006,DAX
app.all('/api/quote', async (req, res) => {
  const { requestCookies, symbolStr } = req.body
  const { data } = await axios.default({
    baseURL: `https://xueqiu.com/v4/stock/quote.json?code=${symbolStr}`,
    method: 'get',
    headers: {
      'Accept': 'application/json, text/plain, */*',
      'Origin': 'https://xueqiu.com',
      'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36',
      'Sec-Metadata': 'destination="", target=subresource, site=same-site',
      'Referer': 'https://xueqiu.com/',
      'Accept-Encoding': 'gzip, deflate, br',
      'Accept-Language': 'zh-CN,zh;q=0.9',
      'Cookie': requestCookies
    }
  })
  res.json(data)
})

const PORT = process.env.PORT || 8080
const HOST = process.env.HOST || 'localhost'
app.listen(PORT, () => {
  console.log(`http://${HOST}:${PORT}`)
})