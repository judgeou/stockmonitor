const puppeteer = require('puppeteer')
// aliyungf_tc=AQAAAO5FWwEvsgUAtgnst6HFjeOWm4qd; device_id=b11a87b85aafa1558af9ed21f90177f4; remember=1; remember.sig=K4F3faYzmVuqC0iXIERCQf55g2Y; xq_a_token=87b4101bc48379f43d6027251c1a088f5dd48442; xq_a_token.sig=BxlntxOqcqV7gyq5zQ60B0ty9ws; xqat=87b4101bc48379f43d6027251c1a088f5dd48442; xqat.sig=I3kvV2JL4mWKck_kXuRjgz3w58U; xq_r_token=399253285cdfdd7fd53465ac081a678b5c450a9a; xq_r_token.sig=m9Wiwgi0jwJlto8zG2xJ0M1S6g4; xq_is_login=1; xq_is_login.sig=J3LxgPVPUzbBg3Kee_PquUfih7Q; u=2920029068; u.sig=tTlcjUYKTMblEoz8dNGQjzOVQpg; snbim_minify=true
async function getPage () {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.setViewport({ width: 1280, height: 720 })
  await page.goto('https://xueqiu.com/');

  await page.waitForSelector('.nav__login__btn')
  await page.click('.nav__login__btn')
  await page.type('[name=username]', '13560313992')
  await page.type('[name=password]', '12345qwert')
  // await page.click('.modal__login__btn')

  return page
}

async function wait (ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms)
  })
}

module.exports = {
  getPage
}
