const HOST = '127.0.0.1'
const PORT = 8080

module.exports = {
  entry: 'src/client/index.js',
  devServer: {
    proxy: {
      '/api': `http://${HOST}:${PORT}`
    }
  }
}